﻿using iPay88.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iPay88.Services
{
    public interface IService
    {
        Task<CardViewModel> getCard(string prefix);
    }
}
