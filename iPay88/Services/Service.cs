﻿using iPay88.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ubiety.Dns.Core.Records.NotUsed;

namespace iPay88.Services
{
    public class Service : IService
    {
        private readonly CardContext _db;
        public Service(CardContext context)
        {
            _db = context;
        }
        public async  Task<CardViewModel> getCard(string prefix)
        {
            var result = await _db.Cards.Where(x => x.Prefix == prefix).Include(b => b.Bank)
                .Include(t => t.CardType).FirstOrDefaultAsync();
             
            if(result != null)
            {
                return new CardViewModel() { Prefix = result.Prefix, BankLogo = result.Bank.Logo, CardLogo = result.CardType.Logo };
            }
            return null;
        }
    }
}
