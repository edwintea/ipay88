﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iPay88.Models;
using iPay88.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using Microsoft.Extensions.Logging;

namespace iPay88.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CardController : Controller
    {
        private readonly ILogger<CardController> _logger;
        private readonly IService _service;
        public CardController(ILogger<CardController> logger, IService service)
        {
            _logger = logger;
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult<CardViewModel>> Get(string prefix)
        {
            var cards = await _service.getCard(prefix);

            if (cards == null) return NotFound();

            return Json(cards);
        }
    }
}
