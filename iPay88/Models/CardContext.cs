﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iPay88.Models
{
    public class CardContext : DbContext
    {
        public CardContext() { }
        public CardContext(DbContextOptions<CardContext> options) : base(options)
        {
        }

        public virtual DbSet<Bank> Banks { get; set; }
        public virtual DbSet<Card> Cards { get; set; }
        public virtual DbSet<CardType> CardTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bank>().ToTable("banks");
            modelBuilder.Entity<Bank>().HasKey(e => e.Id);

            modelBuilder.Entity<CardType>().ToTable("CardTypes");
            modelBuilder.Entity<CardType>().HasKey(e => e.Id);

            modelBuilder.Entity<Card>().ToTable("cards");
            modelBuilder.Entity<Card>().HasKey(e => e.Prefix);

            modelBuilder.Entity<Card>().HasOne<Bank>(e => e.Bank).WithMany(s => s.Cards)
                .HasForeignKey(e => e.BankId);

            modelBuilder.Entity<Card>().HasOne<CardType>(e => e.CardType).WithMany(s => s.Cards)
                .HasForeignKey(e => e.TypeId);
            base.OnModelCreating(modelBuilder);
        }
    }
}
