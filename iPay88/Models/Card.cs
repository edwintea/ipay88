﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iPay88.Models
{
    public class Card
    {
        public string Prefix { get; set; }
        public int BankId { get; set; }
        public int TypeId { get; set; }

        public virtual Bank Bank { get; set; }
        public virtual CardType CardType { get; set; }
    }
}
