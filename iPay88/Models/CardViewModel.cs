﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iPay88.Models
{
    public class CardViewModel
    {
        public string Prefix { get; set; }
        public string BankLogo { get; set; }
        public string CardLogo { get; set; }
    }
}
