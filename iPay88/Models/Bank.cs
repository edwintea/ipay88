﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iPay88.Models
{
    public class Bank
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
        public virtual ICollection<Card> Cards { get; set; }
    }
}
